#pragma once
class Point
{
	int x;
	int y;

public:
	Point() :Point(0, 0)
	{
	}

	Point(const int& x, const int& y) : x(x), y(y)
	{
	}

	int getX() const
	{
		return x;
	}

	int getY() const
	{
		return y;
	}

	void setX(const int& x) {
		this->x = x;
	}

	void setY(const int& y) {
		this->y = y;
	}

	void deplacer(const int& tx, const int& ty);
	void afficher() const;
	double norme() const;

	static double distance(const Point& p1, const Point& p2);
};

