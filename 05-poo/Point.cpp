#include <iostream>
#include <cmath>
#include "Point.h"

using namespace std;

void Point::deplacer(const int& tx, const int& ty)
{
	x += tx;
	y += ty;
}

void Point::afficher() const
{
	cout << "[" << x << "," << y << "]" << endl;
}

double Point::norme() const
{
	return std::sqrt(x * x + y * y);
}

double Point::distance(const Point& p1, const Point& p2)
{
	return std::sqrt(std::pow((p2.x - p1.x), 2) + std::pow((p2.y - p1.y), 2));
}
