#pragma once
#include<string>
#include "Personne.h"

class CompteBancaire
{
protected:
	double solde;
private:
	std::string iban;
	Personne* titulaire;
	static int nombreCompte;

public:

	CompteBancaire(Personne& titulaire) : CompteBancaire(titulaire, 0.0)
	{

	}

	CompteBancaire(Personne& titulaire, double solde) : titulaire(&titulaire), solde(solde)
	{
		nombreCompte++;
		iban = "fr-5962-0000-" + std::to_string(nombreCompte);
	}

	CompteBancaire(const CompteBancaire& cb) = delete;

	std::string getIban() const
	{
		return iban;
	}

	double getSolde() const
	{
		return solde;
	}

	Personne& getTitulaire() const
	{
		return *titulaire;
	}

	bool estPositif() const
	{
		return solde > 0.0;
	}

	void afficher() const;
	void debiter(double valeur);
	void crediter(double valeur);
};

