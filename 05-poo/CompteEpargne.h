#pragma once
#include "CompteBancaire.h"
class CompteEpargne : public CompteBancaire
{
	double taux;

public:

	CompteEpargne(const double& solde, Personne& titulaire, const double& taux) : CompteBancaire(titulaire, solde), taux(taux)
	{ 	}

	double getTaux() const
	{
		return taux;
	}

	void setTaux(const double& taux) {
		this->taux = taux;
	}

	void calculInterets();

};

