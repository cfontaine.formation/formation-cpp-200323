#pragma once
#include<string>
#include <iostream>
#include"Personne.h"
#include "Moteur.h"

class Voiture
{
	// class -> par defaut private
	// Variable d'instance
	std::string marque = "opel";	// en C++ 11, on peut initialiser une variable d'instance
	std::string couleur;
	std::string plaqueIma;

protected:
	int vitesse = 0;		// en C++ 11, on peut initialiser une variable d'instance

private:
	int compteurKm = 20;	// en C++ 11, on peut initialiser une variable d'instance

	// Variable de classe
	static int cptVoiture;

	// Agr�gation
	Personne* proprietaire;

	// Composition
	// Moteur moteur;
	// ou
	Moteur* moteur;

public:
	// Constructeurs

	// Constructeur d�faut
	// Voiture()
	// {
	// }

	// default -> en C++11 permet de cr�er un constructeur par d�faut
	// Voiture() = default;

	// En C++11 Constructeur d�l�gu�
	Voiture() : Voiture("Opel", "Gris", "ww-1234-FR") {
		std::cout << "Constructeur par d�faut voiture" << std::endl;
	};

	// On peut surcharger les constructeurs
	// Liste d'initialisation
	// Voiture(std::string marque, std::string couleur, std::string plaqueIma) : marque(marque), couleur(couleur), plaqueIma(plaqueIma), vitesse(0), compteurKm(20) { // En C++98
	Voiture(std::string marque, std::string couleur, std::string plaqueIma) : marque(marque), couleur(couleur), plaqueIma(plaqueIma) { // En C++ 11 vitesse et compteurKm  sont initialis� directement sur la variable d'instance
		moteur = new Moteur;
		cptVoiture++;
	}

	Voiture(std::string marque, std::string couleur, std::string plaqueIma, int vitesse, int compteurKm) : marque(marque), couleur(couleur), plaqueIma(plaqueIma), vitesse(vitesse), compteurKm(compteurKm) {
		moteur = new Moteur;
		cptVoiture++;
	}

	//Voiture(std::string m, std::string c, std::string p, Personne* proprietaire=nullptr) : marque(m), couleur(c), plaqueIma(p), proprietaire(proprietaire) 
	Voiture(std::string m, std::string c, std::string p, Personne& proprietaire) : marque(m), couleur(c), plaqueIma(p), proprietaire(&proprietaire)
	{
		moteur = new Moteur;
		cptVoiture++;
	}

	// Constructeur par copie
	// Il n'est pas n�cessaire de faire un constructeur par copie, si l'on n'a pas d'attribut allou� dynamiquement (pointeur)
	// si on veut supprimer la possibilit� de faire une copie de l'objet, on peut:
	Voiture(const Voiture& v); // ou Voiture(Voiture& v)

	// Supprimer le constucteur par copie

	// En C++98
	// - uniquement le d�clarer et ne pas donner sa d�finition
	//  Voiture(const Voiture& v);

	// -Le rendre priv�e
	// private:
	//	Voiture(const Voiture& v);
	//public:

	// En C++ 11 -> delete
	//Voiture(const Voiture& v) = delete;


	// Destructeur
	~Voiture();

	// Getter / Setter

	//inline std::string getMarque();
	std::string getMarque() const // Le code plac� dans le .h est implicitement inline
	{
		return marque;
	}

	// Un m�thode peut �tre constante si elle ne modifie pas l'�tat de l'objet
	// Les m�thodes constantes sont les seules que l'on peut utiliser, si l'objet est constant
	std::string getCouleur() const
	{
		return couleur;
	}

	void setCouleur(const std::string& couleur)
	{
		this->couleur = couleur;	 // this permet de lever l'ambiguit� entre une variable d'instance et le param�tre
	}

	std::string getPlaqueIma() const
	{
		return plaqueIma;
	}

	int getVitesse() const
	{
		return vitesse;
	}

	int getCompteurKm()  const
	{
		return compteurKm;
	}

	static int getCptVoiture() // -> Une m�thode de classe ne peut pas �tre const
	{
		return cptVoiture;
	}

	void setProprietaire(Personne* proprietaire)
	{
		this->proprietaire = proprietaire;
	}

	Personne* getProprietaire() const
	{
		return proprietaire;
	}

	//const Personne* getProprietaire() const
	//{
	//	return proprietaire;
	//}

	Moteur* getMoteur() const
	{
		return moteur;
	}


	// M�thode d'instance 
	void accelerer(int vAcc);
	void freiner(int vFrn);
	void arreter();
	bool estArreter() const;
	void afficher() const;

	// M�thode de classe
	static void testMethodeClasse();
	static bool egaliterVitesse(const Voiture& v1, const Voiture v2);

};

