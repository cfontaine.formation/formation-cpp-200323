#include "Voiture.h"
#include <iostream>

using namespace std;

// Initialisation de la variable de classe
int Voiture::cptVoiture = 0;

//Voiture::Voiture()
//{
//	cout << "Constructeur par d�faut" << endl;
//}

//Voiture::Voiture(std::string marque, std::string couleur, std::string plaqueIma)
//{
//	this->marque = marque;
//	this->couleur = couleur;
//	this->plaqueIma = plaqueIma;
//	vitesse = 0;
//	compteurKm = 0;
//}


// Constructeur par copie
Voiture::Voiture(const Voiture& v)
{
	cout << "Constructeur par copie" << endl;
	marque = v.marque;
	couleur = v.couleur;
	plaqueIma = v.plaqueIma;
	vitesse = v.vitesse;
	compteurKm = v.compteurKm;
	proprietaire = v.proprietaire;
	moteur = new Moteur(*(v.moteur));
	cptVoiture++;
}

// Destructeur
Voiture::~Voiture()
{
	cout << "Destructeur" << endl;
	cptVoiture--;
	delete moteur;
}

//inline std::string Voiture::getMarque() 
//{
//	return marque;
//}

void Voiture::accelerer(int vAcc) {
	if (vAcc > 0) {
		vitesse += vAcc;
	}
}

void Voiture::freiner(int vFrn)
{
	if (vFrn > 0) {
		vitesse -= vFrn;
	}
}

void Voiture::arreter()
{
	vitesse = 0;
}

bool Voiture::estArreter() const
{
	return vitesse == 0;
}

void Voiture::afficher() const
{
	cout << "[" << marque << "," << couleur << "," << plaqueIma << "," << vitesse << "," << compteurKm << "]" << endl;
	if (proprietaire) {
		cout << "Proprietaire";
		proprietaire->afficher();
	}
}

void Voiture::testMethodeClasse()
{
	cout << "M�thode de classe" << endl;
	//cout << vitesse << endl;	// pas d'acc�s � une variable d'instance
	//freiner();				// pas d'acc�s au m�thode d'instance
	cout << cptVoiture << endl; // on a acc�s au variable de classe 
}

bool Voiture::egaliterVitesse(const Voiture& v1, const Voiture v2)
{
	// Dans un m�thode de classe, on peut uniquement acc�der � une variable d'instance
	// par l'interm�diaire d' objet pass� en param�tre
	return v1.vitesse == v2.vitesse;
}


