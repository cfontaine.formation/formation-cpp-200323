#pragma once
#include "Voiture.h"
class VoiturePrioritaire : public Voiture
{
	bool gyro;

public:

	VoiturePrioritaire() : gyro(false) { // appel implicite du constructeur par d�faut de la classe m�re
		std::cout << "Constructeur par d�faut de voiture prioritaire" << std::endl;
	}

	// Appel explicite du constructeur 3 param�tres de la classse m�re
	VoiturePrioritaire(std::string marque, std::string couleur, std::string plaqueIma, bool gyro): Voiture(marque,couleur,plaqueIma), gyro(gyro) {

	}

	bool isGyro() const {
		return gyro;
	}

	void alumerGyro() {
		gyro = true;
	}

	void eteindreGyro() {
		gyro = false;
	}

	void afficher();
	void accelerer(int vAcc);
};

