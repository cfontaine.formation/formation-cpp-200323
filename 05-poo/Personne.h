#pragma once
#include<string>

class Personne
{
	std::string prenom;
	std::string nom;

public:
	Personne(const std::string& prenom, const std::string& nom) : prenom(prenom), nom(nom)
	{ }

	std::string getPrenom() const
	{
		return prenom;
	}

	void setPrenom(std::string prenom)
	{
		this->prenom = prenom;
	}

	std::string getNom() const
	{
		return nom;
	}

	void setNom(std::string nom)
	{
		this->nom = nom;
	}

	void afficher() const;


};