#pragma once
class Moteur
{
	int puissance;
	double consommation;

public:
	Moteur(const int& puissance = 2, const double& consommation = 2.5) : puissance(puissance), consommation(consommation)
	{
	}

	int getPuissance() const {
		return puissance;
	}

	double getConsommation() const {
		return consommation;
	}

	void afficher() const;
};

