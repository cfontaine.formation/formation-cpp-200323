#include <iostream>
#include"Voiture.h"
#include "Point.h"
#include"VoiturePrioritaire.h"
#include"CompteBancaire.h"
#include"CompteEpargne.h"
using namespace std;

int main()
{
	// Acc�s � une variable de classe
//	cout << "nombre de voiture = " << Voiture::cptVoiture << endl;
	// Appel d'une m�thode de classe
	Voiture::testMethodeClasse();

	// Instanciation statique ou automatique
	Voiture v1;
	cout << "nombre de voiture = " << Voiture::getCptVoiture() << endl; //Voiture::cptVoiture
	//v1.vitesse = 10;
	cout << v1.getVitesse() << endl; //v1.vitesse

	// Appel d'une m�thode d'instance
	v1.accelerer(30);
	v1.afficher();
	v1.freiner(15);
	v1.afficher();
	cout << v1.estArreter() << endl;
	v1.arreter();
	cout << v1.estArreter() << endl;

	// Instanciation dynamique
	Voiture* v2 = new Voiture;
	cout << "nombre de voiture = " << Voiture::getCptVoiture() << endl;
	//v2->vitesse = 0;
	cout << v2->getVitesse() << endl;
	delete v2;
	cout << "nombre de voiture = " << Voiture::getCptVoiture() << endl;
	Voiture v3("Fiat", "Jaune", "AZ-3456-FR");
	v3.afficher();
	cout << "nombre de voiture = " << Voiture::getCptVoiture() << endl;

	// Constructeur par copie ou recopie
	Voiture v4 = v3; // ou Voiture v4(v3)
	cout << "nombre de voiture = " << Voiture::getCptVoiture() << endl;
	// en C++ 11 Voiture v4={ v3 }; ou  Voiture v4{ v3 };
	v4.afficher();

	// M�thode de classe
	cout << Voiture::egaliterVitesse(v3, v4) << endl;

	// Objet Constant
	const Voiture vCst("Ford", "rouge", "WX-1234-DE");
	cout << vCst.getMarque() << endl;
	vCst.afficher();

	// Agr�gation
	Personne per1("John", "Doe");
	Voiture* v5 = new Voiture("Toyota", "Blanc", "tr-3456-Fr", per1);
	v5->afficher();
	v3.setProprietaire(&per1);
	v3.afficher();
	delete v5;

	// H�ritage
	VoiturePrioritaire vp1;
	vp1.accelerer(30);
	vp1.afficher();
	vp1.alumerGyro();
	cout << vp1.isGyro() << endl;

	VoiturePrioritaire vp2("subaru", "bleu", "fl-2223-fr", true);
	vp2.afficher();
	vp2.alumerGyro();

	Voiture* vp3 = new VoiturePrioritaire("subaru", "rouge", "po-1815-fr", true);
	vp3->afficher();

	// Exercice: Point
	Point a;
	Point b(2, 1);
	Point* c = new Point(3, 4);
	a.afficher();
	a.deplacer(0, 1);
	a.afficher();
	c->afficher();
	cout << c->norme() << endl;
	cout << Point::distance(a, b) << endl;
	delete c;

	// Exercice: Compte Bancaire
	CompteBancaire cb1(per1, 200.0);
	cb1.crediter(120.0);
	cb1.debiter(20.0);
	cb1.afficher();

	Personne per2("Alan", "Smithee");
	CompteBancaire* cb2 = new CompteBancaire(per2, 200.0);
	cb2->afficher();
	delete cb2;

	// Exercice H�ritage: Compte Epargne
	CompteEpargne ce(230.0, per2, 5.0);
	ce.afficher();
	ce.calculInterets();
	ce.afficher();
}
