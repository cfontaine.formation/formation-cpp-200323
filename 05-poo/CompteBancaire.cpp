#include <iostream>
#include "CompteBancaire.h"

using namespace std;

int CompteBancaire::nombreCompte = 0;

void CompteBancaire::afficher() const
{
	cout << "______________________" << endl;
	cout << "Solde: " << solde << endl;
	cout << "Titulaire: ";
	titulaire->afficher();
	cout << "Iban: " << iban << endl;
	cout << "______________________" << endl;
}

void CompteBancaire::debiter(double valeur)
{
	if (valeur) {
		solde += valeur;
	}
}

void CompteBancaire::crediter(double valeur)
{
	if (valeur) {
		solde -= valeur;
	}
}

