#include <iostream>
#include <bitset>

#define VAL_CONST 42; // Macro en C => pour définir une constante  (à éviter en C++)

using namespace std;

// Variable global
int vGlobal = 123;

// Si elle n'est pas initialisée un variable globale est initialisée à 0
// int vGlobal;

int main()
{
	// Déclaration de variable
	int i;

	// Initialisation de variable
	i = 42;
	cout << i << endl;

	// Déclaration  et initialisation devariable
	int mb, mc = 12;
	mb = 12;
	cout << mb << " " << mc << endl;

	// Déclarer et initialiser une variable
	int j = 10;				// C
	int jCpp98(10);			// C++ 98
	int jCpp11{ 10 };		// C++ 11
	int jCpp11B = { 10 };

	cout << j << "  " << jCpp98 << "  " << jCpp11 << "  " << jCpp11B << endl;

	// Littéral entier -> par défaut int
	long l = 10L;			// litéral long -> L
	unsigned int ui = 10U;	// litéral unsigned -> U
	long long lli = 10LL;	// litéral long long -> LL en C++ 11

	// Changement de base
	int decimal = 42;		// base 10 par défaut: décimal
	int hexa = 0xFF14;		// base 16 héxadécimal -> 0x
	int octal = 0341;		// base 8  octal -> 0
	int bin = 0b011101;		// base 2  binaire -> 0b en C++14

	cout << decimal << " " << hexa << " " << hex << hexa << " " << oct << octal << dec << " " << bitset<32>(bin) << endl;

	// Littéral virgule flottante 
	double d1 = 1.5; // 5.		.55;
	double d2 = 1.334e3;

	// Littéral virgule flottante -> par défaut de type double
	float f = 123.4F;		// littéral float -> F
	long double ld = 1.23L;	// littéral long double -> L

	// Littéral booléen
	bool test = true; // false

	// Littéral caractère
	char c = 'a';
	char cHexa = '\x41';	// caractère en hexadécimal
	char cOctal = '\32';	// caractère en octal

	// Variable globale
	cout << vGlobal << endl; // affiche 123
	// On déclare une variable locale qui a le même le nom que la variable globale
	int vGlobal = 12;
	// La variable globale est masquée par la variable locale
	cout << vGlobal << endl; // affiche 12
	// Pour accéder à la variable globale, on utilise l’opérateur de résolution de portée ::
	cout << ::vGlobal << endl; // affiche 123

	// En C pour une constante, on utilise #define
	int v = VAL_CONST;
	cout << v << endl;

	// En C++ -> const
	// Constante
	const double PI = 3.14;
	// PI = 3.1419; // Erreur: on ne peut pas modifier une constante

	// Constexpr C++ 11
	constexpr double pi2 = PI * 2.0;
	cout << pi2 << endl;

	// Typage implicite C++ 11
	// Le type de la variable est définie à partir de l'expression d'initialisation
	auto implicite = 'a'; // implicite de type char
	auto implicite2 = 12.3f; // implicite de type float
	// Attention aux chaine de caratères: avec auto le type sera const char* et pas std::string
	auto impliciteStr = "azerty"; // le type const char *

	int tab[3] = { 1,2,4 };
	auto impl = tab;
	auto implicite3 = PI; // implicite3 de type double const et constexpr ne sont pas pris en compte avec auto
	implicite3 = 3.1;

	// decltype  C++ 11 -> déclarer qu'une variable est de même type qu'une autre
	decltype(c) c2 = 'z';		// c2 est de type char
	decltype(PI) p3 = 3.141;	// p3  est  type double, Comme Pi est une constante p3 est ausi une constante 
	// p3++; // => erreur p3 constant
	decltype(3 * PI + 12.5) p4 = 11.3; // p4 est de type double 

	// Opérateur
	// Opérateur arithmétique
	int aa = 1;
	int bb = 3;
	int res = aa + bb;
	cout << res << " " << bb % 2 << endl; // % => modulo (reste de division entière) uniquement avec des entiers positif

	// Pré-incrémentation
	int inc = 0;
	res = ++inc;
	cout << inc << " " << res << endl; // inc=1 et res=1

	// Post-incrémentation
	inc = 0;
	res = inc++; //res=0 et inc=1
	cout << inc << " " << res << endl;

	// Affectation composée
	res = 12;
	res += 10; // res=res+10

	// Opérateur de comparaison
	int val;
	cin >> val;
	bool tst1 = val > 10; // Une comparaison a pour résultat un booléen
	cout << tst1 << endl;

	// Opérateur logique
	// non
	bool inv = !tst1;
	cout << inv << endl;

	// Opérateur court-circuit && et || (évaluation garantie de gauche à droite)
	// && dés qu'une comparaison est fausse, les suivantes ne sont pas évaluées
	// || dés qu'une comparaison est vrai, les suivantes ne sont pas évaluées
	bool tst2 = val < 50 && val++>20;		// si val < 50 est faux,  val++>20 n'est pas évalué
	cout << tst2 << " " << val << endl;
	bool tst3 = val > 50 || val++ < 20;		// si val > 50 est vrai,  val++>20 n'est pas évalué
	cout << tst3 << " " << val << endl;

	// Opérateur binaire (uniquement avec des entiers)
	// Pour les opérations sur des binaires, on utilise les entiers non signé
	unsigned int b1 = 0b101011;
	cout << ~b1 << " " << bitset<32>(~b1) << endl; // 10100		Complémént: 1 -> 0 et 0 -> 1

	cout << bitset<32>(b1 & 1000) << endl; // 1000		Et bit à bit
	cout << bitset<32>(b1 | 10100) << endl; // 111111	Ou bit à bit
	cout << bitset<32>(b1 ^ 111100) << endl; // 110111  Ou exclusif bit à bit

	// Opérateur de décallage
	cout << b1 << " " << (b1 >> 1); // 10101	Décalage à droite de 1 (-> équivaut à une division par 2)
	cout << b1 << " " << (b1 << 1); // 1010110	Décalage à gauche de 1 (-> équivaut à une multiplication par 2)
	cout << bitset<32>(b1 >> 3) << " " << endl; // 101	Décalage à droite de 3

	// Opérateur sizeof
	cout << sizeof(b1) << endl;		// nombre d'octets de la variable b1 -> 4
	cout << sizeof(bool) << endl;	// nombre d'octets du type bool -> 1

	// Opérateur séquentiel, toujours évalué de gauche->droite
	inc = 0;
	int o = 12;
	res = (inc++, o + 10); // res=22 inc=1
	cout << res << " " << inc << endl;

	// Conversion implicite -> rang inférieur vers un rang supérieur (pas de perte de donnée)
	int convI = 13;
	double convD = 12.3;
	double convRes = convI + convD; //25.3
	cout << convRes << endl;

	// Promotion numérique -> short, bool ou char dans une expression -> convertie automatiquement en int
	short s1 = 2;
	short s2 = 3;
	int s3 = s1 + s2;
	cout << s3 << endl;

	// boolean vers entier
	int convI1 = false; // 0
	int convI2 = true;	// 1

	// entier,nombre à virgule flottante et pointeur => boolean
	bool convB1 = 34;  // true 
	bool convB2 = 0;   // false
	bool convB3 = 12.3; // true
	int* pi = nullptr;
	bool convB4 = pi; // null -> false
	pi = &convI;
	bool convB5 = pi; // différent de null -> true
	cout << convB1 << " " << convB2 << " " << convB3 << " " << convB4 << " " << convB5 << endl;


	// Conversion explicite -> opérateur de cast
	int te = 11;
	int div = 2;
	cout << (te / div) << endl; // 5
	cout << (((double)te) / div) << endl;			// 5.5
	// en C++, avec static_cast, le compilateur fait plus de vérification (sur le type)
	cout << (static_cast<double>(te) / div) << endl; // 5.5

	// Opérateur d'affectation conversion systémathique (implicite et explicite)
	double affD = 12.3;
	int affI = affD; // 12

	// Dépassement de capacité
	int  dep = 300;
	char cDep = dep; // 0001 0010 1100	300 => 300 > 127 ou 255, la valeur maximal d'un char
	cout << static_cast<int>(cDep) << endl;

	// Division par 0
	// pour un entier
	int div1 = 10;
	int div2 = 0;
	// cout << (div1 / div2) << endl; => exception

	// pout un nombre à virgule flottante
	double divD1 = 10.0;
	double divD2 = 0.0;
	cout << (divD1 / divD2) << endl; //+ INF
	// -10.0/0.0 -> donne -INF
	//  0.0 / 0.0 -> donne NAN (Not A number)

	return 0;
}
