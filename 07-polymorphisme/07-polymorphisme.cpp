#include <iostream>
#include "Animal.h"
#include "Chien.h"
#include "Chat.h"

using namespace std;
int main()
{
    //Animal* a1 = new Animal( 2,3000); // La classe Animal est abstraite, elle ne peut plus être instensié 
    //a1->afficher();
    //a1->emmetreSon();

    Chien* ch1 = new Chien("Rolo", 4, 8000);
    ch1->afficher();
    ch1->emmetreSon();
   
    Animal* a2 = new Chien("Laika", 2, 3000);
    a2->afficher();
    a2->emmetreSon();    // virtual appel de la méthode emmetreSon de Chien

    //Chien* ch2 = (Chien*)a2;
    Chien* ch2 = dynamic_cast<Chien*>(a2);
    if (ch2) { // ou ch2!=nulptr
        cout << ch2->getNom() << endl;
    }

    //Chien* ch3 = dynamic_cast<Chien*>(a1); // retourner 0 ou nullptr et exception bad_cast
    //if (ch3) {
    //	ch3->emmetreSon();
    //	cout << ch3->getNom() << endl;
    //}

    // typeid -> retourne un objet de type type_info
   // cout << typeid(*a1).name() << endl;
    cout << typeid(*a2).name() << endl;
    //if (typeid(*a1) != typeid(*a2)) {
    //    cout << "type différent" << endl;
    //}

    Animal* a3 = new Chat(5,3500);
    a3->emmetreSon();

   // delete a1;
    delete a2;
    delete a3;
    delete ch1;
    delete ch2;
 //   delete ch3;
}
