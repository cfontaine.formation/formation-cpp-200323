#pragma once
#include "Animal.h"
class Chat : public Animal
{
	int nbVie = 9;

public:
	Chat(int age, int poid) :Animal(age, poid) {}

	int getNbVie()const
	{
		return nbVie;
	}

	void setNbVie(int nbVie) 
	{
		this->nbVie = nbVie;
	}

	void emmetreSon() override;
};

