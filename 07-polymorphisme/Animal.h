#pragma once
class Animal
{
	int age;
	int poid;

public:
	Animal(int age, int poid) :age(age), poid(poid) {}

	// virtual ~Animal() = 0; // si on veut cr�er, une classe abstraite sans avoir de m�thode virtuelle pure dans la classe
							  // |-> on en cr�e une avec le destructeur

	int getAge() const {
		return age;
	}

	void setAge(const int& age) {
		this->age = age;
	}

	int getPoid() const {
		return poid;
	}

	void setPoid(const int& poid) {
		this->poid = poid;
	}

	virtual void afficher();

	virtual void emmetreSon() = 0;	// m�thode abstraite -> virtuelle pure, la classe devient abstraite
};

