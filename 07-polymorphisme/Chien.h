#pragma once
#include <string>
#include "Animal.h"

class Chien : public Animal
{
	std::string nom;

public:
	Chien(std::string nom, int age, int poid) : Animal(age, poid), nom(nom) {}

	std::string getNom() const {
		return nom;
	}

	// avec virtual, le type de retour doit �tre le m�me
	// sans virtual, la m�thode red�finie, peut avoir un type de retour diff�rent
	void emmetreSon() override final; // en c++11 -> override entraine la v�rification par le compilateur qu'il s'agit bien d'une red�finition
									  // en c++11 -> final interdire pour les sous-classes une red�finition "vituelle"
	void afficher() override;
};

