
#include <iostream>
#include "Point.h"
#include "Fraction.h"
using namespace std;

int main()
{
    Point a(1, 1);
    Point b(2, -3);

    Point c = -a;
    cout << a << " " << b << " " << c << endl;

    Point d = a + b;
    cout << d << endl;

    Point e = a * 3;
    cout << e << endl;;

    Point f=e++;
    cout << e <<" "<<f << endl;

    f = ++e;
    cout << e << " " << f << endl;

    std::cout << f[0] << std::endl;
    std::cout << f[1] << std::endl;
    //std::cout << f[2] << std::endl;

    Point a1(1, 1);
    std::cout << (a1 == a) << std::endl;
    std::cout << (a1 != a) << std::endl;

    Fraction f1(4, 8);
    Fraction f2(1, 2);
    Fraction r = f1 + f2;
    std::cout <<r << std::endl;
    r = f1 * f2;
    std::cout << r<< std::endl;
    r = f1 * 2;
    std::cout << r << std::endl;
    cout << (f1 == f2) << endl;
}

