#pragma once
class MaClasse
{
	int val;

public:
	MaClasse(int val) : val(val) {

	}

	int getVal() const
	{
		return val;
	}

	void afficher() const;

	// Fonction amie
	friend void fonctionAmie(MaClasse&);

	// Classe amie
	friend class ClasseAmie;
};

