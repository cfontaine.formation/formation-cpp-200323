
#include <iostream>
#include "MaClasse.h"
#include "ClasseAmie.h"

// fonctionAmie peut acc�der � la variable d'instance val de la classe MaClasse car elle est d�clar� amie
void fonctionAmie(MaClasse& m)
{
	m.val = 123;
}

int main()
{
	// Fonction amie
	MaClasse mc(1);
	mc.afficher();
	fonctionAmie(mc);
	mc.afficher();

	// Classe amie
	ClasseAmie ca(mc);
	ca.traitement();
	mc.afficher();

}

