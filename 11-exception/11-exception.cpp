#include <iostream>
#include <string>
#include "AgeNegatifException.h"

using namespace std;

void etatCivil(int age);
void traitementAge(int age);
void neLancePasDException() noexcept;

int main()
{
	int age;
	cin >> age;
	try 
	{
		etatCivil(age);
		cout << "le reste du programme" << endl;
	}
	catch (AgeNegatifException& e)
	{
		cerr << "exception personnalisée:" <<e.what() <<endl;
	}
	catch (int a)
	{
		cerr <<"exception int: age négatif =" << a<<endl;
	}
	catch (const char* str)
	{
		cerr << "exception const char*" << str << endl;
	}
	catch (...)
	{
		cerr << "autre type d'exception" << endl;
	}
	cout << "fin de programme" << endl;
}

void etatCivil(int age)
{
	cout<< "début Etat Civil"<<endl;
	try {
		traitementAge(age);
	}
	catch (AgeNegatifException& e) {
		cout << "Traitement partiel age: état civil" << endl;
		cerr << e.what() << endl;
		//throw; // relance de l'exception
		throw age;
	}
	cout << "fin Etat Civil" << endl;
}

void traitementAge(int age) throw(int,char,const char*) // en c++98, pas besoin en c++11 
{
	cout << "Début traitement Age" << endl;
	if (age < -3) {
		throw AgeNegatifException(age);
	}
	else if (age == 0) {
		throw "Age égal à zéro";
	}
	else if (age == -1) {
		throw 'u';
	}
	else if (age == -2) {
		throw age;
	}
	cout << "Fin traitement Age" << endl;
}

void neLancePasDException() noexcept //throw() // en C++98
{
	cout<<"test"<<endl;
}