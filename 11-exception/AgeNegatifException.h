#pragma once
#include<exception>
#include<string>
class AgeNegatifException : public std::exception
{
	std::string message;

public:

	AgeNegatifException(int age) : message("L'age est negatif: " + std::to_string(age)) {}

	const char* what();
};

