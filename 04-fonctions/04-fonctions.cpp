#include <iostream>
#include <string>
#include "MesFonctions.h"
#include "MesStructures.h"

using namespace std;

// Déclaration des fonctions => déplacer dans le fichier .h
//int somme(int, int);
//void afficher(int a);

const int testVarExtern = 456;

static double pi = 3.14;	 // avec static la variable n'est visible que dans ce fichier

int main(int argc, char* argv[])
{
	// Appel de methode
	int res = somme(1, 3);

	// Appel de methode (sans retour)
	afficher(res);
	afficher(5);

	// Paramètre passé par valeur
	// C'est une copie de la valeur du paramètre qui est transmise à la méthode
	int v = 1;
	testParamValeur(v);
	std::cout << v << endl;

	// Paramètre passé par adresse
	// La valeur de la variable passée en paramètre est modifiée, si elle est modifiée dans la méthode
	testParamAdresse(&v);	// on passe en paramètre l'adresse de la variable p
	std::cout << v << endl;

	// Les tableaux peuvent être passer par adresse et pas par valeur
	int t[] = { 1,6,4,6,3 };
	afficherTab(t, 5);	// le nom du tableau correspond à un pointeur sur le premier élément du tableau

	// Pointeur de pointeur
	int* tp = nullptr;
	creerTab(&tp, 3, 2);
	afficherTab(tp, 3);

	// Paramètre passé par référence
	testParamReference(v);
	std::cout << v << endl;

	// Avec une référence constante, on peut aussi passer des littérals
	testParamReferenceConst(v);
	testParamReferenceConst(23);

	// Paramètre par défaut (uniquement pour les paramètres passés par valeur et référence constante)
	testParamDefault(2);
	testParamDefault(3, true);
	testParamDefault(3, true, 'z');

	// Exercice Fonction paire
	std::cout << even(2) << endl;
	std::cout << even(3) << endl;

	// avec static la variable n'est visible que dans ce fichier
	int res2 = doubler(v);// 2*v

	constexpr int max = maximum(3, 7); // 7
	int max2 = maximum(v, 2); // fonction inline normal 

	// Surcharge de fonction
	std::cout << multiplier(1, 2) << endl;
	std::cout << multiplier(1.5, 2.5) << endl;
	std::cout << multiplier(1, 2.5) << endl;
	std::cout << multiplier(1, 2, 5) << endl;

	std::cout << multiplier(1, 3.6f) << endl;	// Convertion des caractères en entier
	std::cout << multiplier('A', 'a') << endl;	// Convertion du float en double

	// Les arguments double& et const double& sont considérés comme différents	
	int a = 12;
	std::cout << multiplier(&a, 2) << endl;
	std::cout << multiplier(3.0, 2) << endl;
	double b = 1.2;
	std::cout << multiplier(b, 2) << endl;

	// Fonction récurssive
	std::cout << factorial(3) << endl;

	// Paramètre de la fonction main
	for (int i = 0; i < argc; i++) {
		std::cout << argv[i] << endl;
	}

	// Pointeur de fonction
	ptrf pf = addition;
	std::cout << (*pf)(1.2, 3.5) << endl;
	std::cout << pf(1.2, 3.5) << endl;

	std::cout << calcul(4.6, 6.7, addition) << endl;
	std::cout << calcul(3.1, 2.2, multiplier) << endl;

	// Expression Lambda

	// C++ 11 => expression lambda
	// [liste de capture](paramètre) -> type_retour {corps de la fonction} 
	// Avec les pointeurs de fonction la liste de capture doit obligatoirement être vide []
	std::cout << calcul(3.1, 2.2, [](double v1, double v2) -> double {return v1 - v2; }) << endl;

	// C++ 14 => expression lambda généralisée
	// [liste de capture](paramètre){corps de la fonction} 
	std::cout << calcul(3.1, 2.2, [](double v1, double v2) {return v1 - v2; }) << endl;

	// liste de capture pour utiliser des variables déclarées dans l'expression lambda

	// - par valeur
	int ca = 2;
	auto exp = [ca]() {return ca * 2.0; };
	std::cout << exp() << endl;

	// - par référence
	auto exp2 = [&ca]() {return ++ca; };
	std::cout << ca << endl;
	std::cout << exp2() << endl;
	std::cout << ca << endl;

	// - = correspont à la capture par VALEUR de toutes les variables déclarées
	auto exp3 = [=]() {return ca * 2.0 + b; };
	std::cout << exp3() << endl;

	// - & correspont à la capture par REFERENCE de toutes les variables déclarées 
	auto exp4 = [&]() {
		b *= 3.0;
		return ca++ * 2.0 + b;
	};
	std::cout << ca << " " << b << endl;
	std::cout << exp4() << endl;
	std::cout << ca << " " << b << endl;



	// /!\ une fonction ne doit pas retourner une référence ou un pointeur sur une variable locale à une fonction
	string* str = testRetour(5);
	cout << str << " " << *str << endl;
	delete str;

	// Classe de mémorisation
	// static
	testMemStatic();
	testMemStatic();
	testMemStatic();

	// extern
	cout << testVarExtern << endl;
	testMemExtern();

	// Fonction à nombre variable de paramètre  
	// Hérité du C => Problème pas de vérification de type
	cout << moyenne(2,1,2) << endl;
	cout << moyenne(4, 4, 7, 1, -3) << endl;

	// C++ 11 => initializer_list
	cout << moyenneCpp11({ 1,2}) << endl;
	cout << moyenneCpp11({ 1,3,5,6 }) << endl;

	// Exerice tableau
	menu();

	// struct Contact c1; // en C
	Contact c1;	// en C++ struct n'est pas nécéssaire
	c1.prenom = "John";
	c1.nom = "Doe";
	c1.telephone = "0320000000";
	c1.age = 37;
	c1.afficher();

	Contact c2 = { "Jane","Doe","0320000000" ,29 };
	c2.afficher();

	//  
	Contact c3 = c2;
	c3.afficher();
	c2.age = 30;
	c2.afficher();
	c3.afficher();

	// On teste l'égalité de 2 structures champs par champs
	bool test = c1.age == c2.age && c1.nom == c2.nom && c1.prenom == c2.prenom && c1.telephone == c2.telephone;
	cout << test << endl;

	Contact* ptrC = new Contact;
	(*ptrC).nom = "Smithee";
	ptrC->prenom = "alan";
	ptrC->telephone = "0327000000";
	ptrC->age = 40;
	ptrC->afficher();
	delete ptrC;

	const Contact cCst = { "Jane","Doe","0320000000" ,29 };
	cout << cCst.age << endl;
	//cCst.nom = "Durand";
	cCst.age = 32; // champs mutable -> on peut le modifier même si l'objet est constant

	// Chaine de caractère
	// en C, un chaine de caractère est un tableau de caractère qui est terminé par un caractère terminateur '\0'
	const char* strCstC = "Hello";	// Chaine constante -> const char*
	// strCstC[0] = 'h'; // => erreur
	cout << strCstC << endl;

	char strC[] = "Hello";
	strC[0] = 'h';
	// Nombre de caractères de la chaine +1 pour le terminateur \0
	cout << strC << " " << sizeof(strC) << endl;

	// En C++ => string
	string strCpp = "Hello";
	cout << strCpp << endl;
	string str1 = string("bonjour");
	string str2 = string(10, 'a');

	string* str3 = new string("azerty");
	cout << *str3 << endl;
	delete str3;

	// Concaténation
	strCpp += " World";		// Opérateur + et += concaténation de chaine
	cout << strCpp << endl;
	strCpp.append(" !!!!");	// append -> permet d'ajouter la chaine passée en paramètre en fin de chaine
	cout << strCpp << endl;
	strCpp.push_back('?');	// push_back -> ajoute le caractère passé en paramètre en fin de chaine
	cout << strCpp << endl;

	// Comparaison
	// On peut utiliser les opérateurs de comparaison avec les chaines de caractères
	if (str1 == str2)
	{
		cout << "==" << endl;
	}
	else
	{
		cout << "!=" << endl;
	}

	if (str2 < str1) {
		cout << "inférieur" << endl;
	}

	cout << strCpp.length() << endl;	// length -> nombre de caractères de la chaine (idem pour size)
	cout << strCpp.empty() << endl;		// empty -> retourne true si la chaine est vide
	string str5 = " ";
	cout << str5.length() << endl;
	cout << str5.empty() << endl;

	// Accès à un cararactère de la chaine [] ou at
	cout << strCpp[1] << endl;
	cout << strCpp.at(1) << endl;
	// Si l'indice dépasse la taille de la chaine
	//cout << strCpp[40] << endl;    // [] -> erreur du programme
	//cout << strCpp.at(40) << endl; // at -> une exception est lancée

	// Extraire une sous-chaine -> substr
	cout << strCpp.substr(5) << endl;		// retourne une  sous-chaine qui commence à l'indice 5 et jusqu'à la fin
	cout << strCpp.substr(6, 2) << endl;	// retourne une  sous-chaine de 5 caractères qui commence à l'indice 6

	// Insérer une sous-chaine -> insert
	strCpp.insert(5, "-----");	// insère la chaine passée en paramètre à la position 5
	cout << strCpp << endl;

	// remplacer une partie de la chaine par une sous-chaine -> replace
	strCpp.replace(6, 1, "_?!");  // remplace à la position 6 , 1 caractère par la chaine de caractère passé en paramètre par _?!
	cout << strCpp << endl;

	// Supprimer une partie de la chaine -> erase
	strCpp.erase(5, 7);		// supprimer 7 caractères de la chaine à partir de la position 5
	cout << strCpp << endl;
	strCpp.erase(12);		// supprimer les caractères de la chaine à partir de la position 12 jusqu'à la fin de la chaine
	cout << strCpp << endl;

	// Recherche de la position de la chaine ou du caractère passée en paramètre dans la chaine str
	// -> find
	cout << strCpp.find('o') << endl;		// à partir du début de la chaine
	cout << strCpp.find('o', 5) << endl;		// à partir de la position 5
	// à partir de la position 8, pas de caractère => retourne la valeur std::string::npos
	cout << strCpp.find('o', 8) << " " << string::npos << endl;

	// -> rfind
	// idem find, mais on commence à partir de la fin de la chaine et on va vers le debut
	cout << strCpp.rfind('o') << endl;


	// Iterateur = > objet qui permet de parcourir la chaine(un peu comme un pointeur)
	// begin => retourne un iterateur sur le début de la chaine
	// end => retourne un iterateur sur la fin de la chaine
	for (string::iterator it = strCpp.begin(); it != strCpp.end(); it++) {	  // it++ permet de passer au caractère suivant
		cout << *it << endl;	// *it permet d'obtenir le caractère "pointer" par l'itérateur
		//	*it = 'a';			// on a accés en lecture et en écriture
	}
	cout << strCpp << endl;

	// rbegin et rend idem  mais l'itérateur part de la fin et va vers le début de la chaine
	for (auto it = strCpp.rbegin(); it != strCpp.rend(); it++) {	// it++ permet de passer au caractère précédent
		cout << *it << endl;
	}

	// en C++11 
	// Parcourir une chaine complétement
	for (auto chr : strCpp) {
		cout << chr << endl;
	}

	// en C++11
	// Différence rbegin / crbegin
	// crbegin -> on a accès uniquement en lecture par l'intermédiaire de l'ittérateur
	for (auto it = strCpp.cbegin(); it != strCpp.cend(); it++) {
		cout << *it << endl;
	}

	// Convertion string en chaine C (const *char)	-> c_str
	cout << strCpp.c_str() << endl;

	strCpp.clear();					// Efface les caractères contenus dans la chaine
	cout << strCpp.empty() << endl;	// empty => retourne true si la chaine est vide

	// Exercice: inverser
	cout << inverser("Bonjour") << endl;

	// Exercice: Accronyme
	cout << acronyme("Comité international olympique") << endl;
	cout << acronyme("Organisation du traité de l'Atlantique Nord") << endl;
	return 0;
}

// Définition des fonctions => déplacer dns le fichier .cpp
//int somme(int a, int b)
//{
//	return a + b;
//}
//
//void afficher(int a)
//{
//	cout << a << endl;
//}
