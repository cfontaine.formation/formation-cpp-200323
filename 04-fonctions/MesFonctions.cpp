﻿#include "MesFonctions.h"
#include <iostream>
#include <cstdarg>

using namespace std;

// Fichier .cpp  -> contient les Définitions des fonctions

int somme(int a, int b)
{
	return a + b;
}

void afficher(int a)
{
	cout << a << endl;
	// return;
}

void testParamValeur(int val)
{
	cout << val << endl;
	val = 42;
	cout << val << endl;
}

// En utilisant le passage de paramètres par adresse, on modifie réellement la variable qui est passée en paramètre
void testParamAdresse(int* v1)
{
	cout << *v1 << endl;
	*v1 = 42;
	cout << *v1 << endl;
}

void afficherTab(int* tab, int size)
{
	cout << "[ ";
	for (int i = 0; i < size; i++) {
		cout << tab[i] << " ";
	}
	cout << "]" << endl;
}

// void creerTab(int* tab[], int size, int valInit)
void creerTab(int** tab, int size, int valInit)
{
	*tab = new int[size];
	for (int i = 0; i < size; i++) {
		(*tab)[i] = valInit;
	}
}

// Passage de paramètre par référence
// Comme avec le passage de paramètres par adresse, on modifie réellement la variable qui est passée en paramètre
void testParamReference(int& v2)
{
	cout << v2 << endl;
	v2 += 20;
	cout << v2 << endl;
}

void testParamReferenceConst(const int& v2)
{
	cout << v2 << endl;
	//v2 = 10;
}

// Les paramètres par défaut sont uniquement défini dans la déclaration de la fonction (.h)
void testParamDefault(int i, const bool& b, char c)
{
	cout << i << " " << b << " " << c << endl;
}

// Fonction Paire
// Écrire une fonction even qui prend un entier en paramètre
// Elle retourne vrai si il est paire
bool even(int valeur)
{
	return valeur % 2 == 0;
}

// Surcharge de fonction
// Plusieurs fonctions peuvent avoir le même nom
// Ces arguments doivent être différents en nombre ou/et en type pour qu'il n'y ai pas d'ambiguité pour le compilateur
int multiplier(int v1, int v2)
{
	cout << "2 entiers" << endl;
	return v1 * v2;
}

double multiplier(double v1, double v2)
{
	cout << "2 doubles" << endl;
	return v1 * v2;
}

double multiplier(int v1, double v2)
{
	cout << "un entier, un double" << endl;
	return v1 * v2;
}

int multiplier(int v1, int v2, int v3)
{
	cout << "3 entiers" << endl;
	return v1 * v2 * v3;
}

int multiplier(int* v, int w)
{
	cout << "1 pointeurs entier 1 entier" << endl;
	return *v * w;
}

int multiplier(double& d, int i)
{
	cout << "une référence un entier" << endl;
	return d * i;
}

int multiplier(const double& d, int i)
{
	cout << "une référence constante et un entier" << endl;
	return d * i;
}


// Fonction recursive
int factorial(int n) // factoriel= 1* 2* … n
{
	if (n <= 1) // condition de sortie
	{
		return 1;
	}
	else
	{
		return factorial(n - 1) * n;
	}
}


// Pointeur de fonction
double addition(double a, double b)
{
	return a + b;
}

double calcul(double d1, double d2, ptrf f)
{
	if (f != nullptr) {
		//	return (*f)(d1, d2);
		return f(d1, d2);
	}
}

// /!\ une fonction ne doit pas retourner une référence ou un pointeur sur une variable locale à une fonction
std::string* testRetour(int n)
{
	//string tmp = string(n, 'a');		// variable locale ==> erreur
	//return &tmp;
	string* tmp = new string(n, 'a');
	return tmp;
}

// Classe de mémorisation static
void testMemStatic()
{
	// Entre 2 exécutions val conserve sa valeur
	static int i = 100;	// par défaut initialisé à 0		
	i++;				// i n'est visible que dans la fonction
	cout << i << endl;
}

// Classe de mémorisation extern
void testMemExtern()
{
	cout << testVarExtern << endl;
}

// Nombre d'arguments variable
// en C et c++98
double moyenne(int nbarg, ...)
{
	va_list vl;
	va_start(vl, nbarg);
	double somme = 0.0;
	for (int i = 0; i < nbarg; i++) {
		somme += va_arg(vl, int);
	}
	va_end(vl);	// nettoyage des arguments
	return nbarg == 0 ? 0.0 : somme / nbarg;
}
// C++11 => initializer_list
double moyenneCpp11(std::initializer_list<int> valeurs)
{
	double somme = 0.0;
	for (auto v : valeurs) {
		somme += v;
	}
	return valeurs.size() ? somme / valeurs.size() : 0.0;
}

// Exercice Inversion de chaine
// Écrire la fonction inverser qui prend en paramètre une chaine et qui retourne la chaine avec les caractères inversés
std::string inverser(const std::string& str)
{
	string tmp = "";
	for (auto it = str.rbegin(); it != str.rend(); it++)
	{
		tmp.push_back(*it);
	}
	return tmp;

}

// Acronyme
// Faire une méthode Acronyme qui prend en paramètre une phrase et qui retourne un acronyme
// Les mots de la phrase sont pris en compte s'ils contiennent plus de 2 lettres
//		Comité international olympique					→ CIO
//		Organisation du traité de l’Atlantique Nord     → OTAN
std::string acronyme(const std::string& str)
{
	string acr = "";
	auto itDebutMot = str.begin();
	for (auto it = str.begin(); it != str.end(); it++)
	{
		if (*it == ' ' || *it == '\'' || it == str.end() - 1)
		{
			if (it - itDebutMot > 2)
			{
				acr.push_back(*itDebutMot);
			}
			itDebutMot = it + 1;
		}
	}
	return acr;
}


int saisieTab(int** tab)
{
	int size;
	cout << "Taille de tableau : ";
	cin >> size;
	if (size > 0)
	{
		*tab = new int[size];
		for (int i = 0; i < size; i++)
		{
			cout << "tab[" << i << "]=";
			cin >> (*tab)[i];
		}
	}
	else {
		size = 0;
		*tab = nullptr;
	}
	return size;
}

void calculTab(const int tab[], const int& size, int& maximum, double& moyenne)
{
	maximum = tab[0];
	double somme = 0.0;
	for (int i = 0; i < size; i++)
	{
		if (tab[i] > maximum) 
		{
			maximum = tab[i];
		}
		somme += tab[i];
	}
	moyenne = somme / size;
}

int choixMenu()
{
	int choix;
	cout << "1 - Saisir le tableau" << endl;
	cout << "2 - Afficher le tableau" << endl;
	cout << "3 - Afficher le maximum et la moyenne" << endl;
	cout << "0 - Quitter " << endl;
	do {
		cout << "Choix = ";
		cin >> choix;
	} while (choix < 0 || choix >3);
	return choix;
}

void menu()
{
	int* tab = nullptr;
	int size = 0;
	int choix = 0;
	do {
		choix = choixMenu();
		switch (choix)
		{
		case 1:
			delete[] tab;
			size = saisieTab(&tab);
			break;
		case 2:
			if (tab != nullptr) {
				afficherTab(tab, size);
			}
			break;
		case 3:
			if (tab != nullptr) {
				int maximum;
				double moyenne;
				calculTab(tab, size, maximum, moyenne);
				cout << "maximum=" << maximum << " moyenne=" << moyenne << endl;
			}
			break;
		}
	} while (choix != 0);
}
