// Fichier .h -> contient la d�claration des fonctions

#pragma once	// Indique au compilateur de n'int�grer le fichier d�en-t�te qu�une seule fois, lors de la compilation d�un fichier de code source
// ou
//#ifndef MES_FONCTIONS_H 
//#define MES_FONCTIONS_H

#include <string>
#include <initializer_list>
#include "MesStructures.h"

// d�claration d'une variable qui a �t� d�finie dans main.cpp. Il n' y a pas d'allocation m�moire
extern const int testVarExtern;	

// extern double pi;	// pi est static dans 04-fonctions.cpp, on ne peut l'utiliser que dans ce fichier;

// D�claration d'une fonction
int somme(int, int);

// D�claration d'une fonction sans retour => void
void afficher(int a);

// Passage de param�tre par valeur
void testParamValeur(int val);

// Passage de param�tre par adresse
// En utilisant le passage de param�tres par adresse, on modifie r�ellement la variable qui est pass�e en param�tre
void testParamAdresse(int* v1);

// Passage d'un tableau, comme param�tre d'une fonction
// On ne peut pas passer un tableau par valeur uniquement par adresse
//void afficherTab(int tab[], int size);
void afficherTab(int* tab, int size);

// Pointeur de pointeur
//void creerTab(int* tab[], int size, int valInit);
void creerTab(int** tab, int size, int valInit);

// Passage de param�tre par r�f�rence
// Comme avec  le passage de param�tres par adresse, on modifie r�ellement la variable qui est pass�e en param�tre
void testParamReference(int& v2);

void testParamReferenceConst(const int& v2);

// On peut d�finir des valeurs par d�faut pour les param�tres. Ils doivent se trouver en fin de liste des arguments
// On place les valeurs par d�faut dans la d�claration des fonctions
void testParamDefault(int i, const bool& b=false, char c='a');

// Exercice fonction pair
bool even(int valeur);

// Pour les fonctions inline, on place le code de la fonction dans le fichier.h
inline int doubler(int a) {
	return 2 * a;
}

// Les fonctions constexpr sont implicitement inline
constexpr int maximum(int a, int b) {
	return a > b ? a : b;
}

// Surcharge de m�thode
int multiplier(int v1, int v2);
double multiplier(double v1, double v2);
double multiplier(int v1, double v2);
int multiplier(int v1, int v2,int v3);
int multiplier(int* v, int w);
int multiplier(double& d, int i);
int multiplier(const double& d, int i);
//int multiplier(int& v, int w); // ambigue
//int multiplier(const int& v, int w); // ambigue

// R�cursivit�
int factorial(int n);

// Pointeur de fonction
// typedef permet de d�finir des synonymes de types -> typedef type synonyme_type;
typedef double (*ptrf)(double, double);
double addition(double a, double b);
double calcul(double d1, double d2, ptrf f);

// ERREUR => fonctions qui retournent une r�f�rence ou un pointeur sur une variable locale
std::string* testRetour(int n);

// Test de classe de m�morisation
void testMemStatic();
void testMemExtern();

// Fonction � nombre variable de param�tre
// h�rit� du C
double moyenne(int nbarg, ...);

// en C++ -> initializer_list
double moyenneCpp11(std::initializer_list<int> valeurs);

// Exercice Tableau
int saisieTab(int** tab);

void calculTab(const int tab[], const int& size, int& maximum, double& moyenne);

void menu();

int choixMenu();

// Exercices chaine de caract�re
std::string inverser(const std::string &str);
std::string acronyme(const std::string& str);

//#endif