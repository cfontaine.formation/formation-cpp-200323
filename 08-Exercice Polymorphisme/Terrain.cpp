#include "Terrain.h"

bool Terrain::ajoutForme(Forme& forme)
{
	{
		if (nombreForme < 10) {
			formes[nombreForme] = &forme;
			nombreForme++;
			return true;
		}
		return false;
	}
}

double Terrain::surfaceTotal()
{
	double surface = 0.0;
	for (int i = 0; i < nombreForme; i++) {
		surface += formes[i]->calculSurface();
	}
	return surface;
}

double Terrain::surfaceTotal(const Couleur& couleur)
{
	double surface = 0.0;
	for (int i = 0; i < nombreForme; i++) {
		if (formes[i]->getCouleur() == couleur) {
			surface += formes[i]->calculSurface();
		}
	}
	return surface;
}
