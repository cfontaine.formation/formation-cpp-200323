#include <iostream>
using namespace std;

int main()
{
	// Moyenne
	// Saisir 2 nombres entiers et afficher la moyenne dans la console
	int a, b;
	cout << "Saisir 2 nombre entier: ";
	cin >> a >> b;
	double moyenne = static_cast<double>(a + b) / 2; // (a+b)/2.0;
	cout << "Moyenne=" << moyenne;

	// Nombre pair ou impair
	// Créer un programme qui indique, si le nombre entier saisie dans la console est pair ou impair
	int val;
	cout << "Saisir un nombre entier: ";
	cin >> val;
	if (!(val & 1)) {//!(val % 2)) { //val % 2 == 0
		cout << "Pair" << endl;
	}
	else {
		cout << "Impair" << endl;
	}

	// Interval
	// Saisir un nombre et dire s'il fait parti de l'intervalle - 4 (exclus)et 7 (inclus)
	int v;
	cout << "Saisir un nombre entier: ";
	cin >> v;
	if (v > -4 && v <= 7) {
		cout << v << " fait parti de l'interval" << endl;
	}

	// switch
	const int LUNDI = 1;
	int jours;
	std::cout << "Saisir un nombre entre 1 et 7 ";
	cin >> jours;
	switch (jours)
	{
	case LUNDI:
		std::cout << "Lundi" << std::endl;
		break;
	case LUNDI + 5:
	case LUNDI + 7:
		std::cout << "Week end !" << std::endl;
		break;
	default:
		std::cout << "Un autre jour" << std::endl;
	}

	// Opérateur ternaire
	cout << "Saisir un nombre entier: ";
	double va;
	cin >> va;
	double abs = va > 0.0 ? va : -va;
	cout << abs << endl;

	// Calculatrice
	// Faire un programme calculatrice
	//	Saisir dans la console
	//	- un double
	//	- une caractère opérateur qui a pour valeur valide : + - * /
	//	- un double

	// Afficher:
	// - Le résultat de l’opération
	// - Une message d’erreur si l’opérateur est incorrecte
	// - Une message d’erreur si l’on fait une division par 0
	double v1, v2;
	char op;
	cout << "Saisir un nombre ,un operateur et un nombre" << endl;
	cin >> v1 >> op >> v2;
	switch (op) {
	case '+':
		cout << v1 << " + " << v2 << " = " << (v1 + v2) << endl;
		break;
	case '-':
		cout << v1 << " - " << v2 << " = " << (v1 - v2) << endl;
		break;
	case '*':
		cout << v1 << " * " << v2 << " = " << (v1 * v2) << endl;
		break;
	case '/':
		if (v2 == 0) {
			cout << "division par 0" << endl;
		}
		else {
			cout << v1 << " / " << v2 << " = " << (v1 / v2) << endl;
		}
		break;
	default:
		cout << op << " n'est pas un operateur";
	}

	// Instructions de branchement inconditionnel
	for (int i = 0; i < 10; i++) {
		if (i == 3) {
			break;	// break => termine la boucle
		}
		cout << "i=" << i << endl;
	}

	for (int i = 0; i < 10; i++) {
		if (i == 3) {
			continue;	// continue => on passe à l'itération suivante
		}
		cout << "i=" << i << endl;
	}

	// goto
	for (int i = 0; i < 10; i++) {
		for (int j = 0; j < 10; j++) {
			if (i == 3) {
				goto EXIT_LOOP;	// Utilisation de goto pour sortir de 2 boucles imbriquées
			}
			cout << "i=" << i << " j=" << j << endl;
		}

	}
EXIT_LOOP:

	// Table de multiplication
	// Faire un programme qui affiche la table de multiplication pour un nombre entre 1 et 9
	//	1 X 4 = 4
	//	2 X 4 = 8
	//	…
	//	9 x 4 = 36
	//	Si le nombre passé en paramètre est en dehors de l’intervalle 1 à 9, on arrête sinon on redemande une nouvelle valeur

	int mul;
	for (;;) { // while(1)	  => boucle infinie
		cin >> mul;
		if (mul < 1 || mul>9) {
			break;
		}
		for (int i = 1; i < 10; i++) {
			cout << i << " x " << mul << " = " << mul * i << endl;
		}
	}

	// C++17 déclaration dans un if et un switch
	int va1, vb1;
	cin >> va1 >> vb1;
	if (int n = 1; va1 < vb1) {
		cout << ++n << endl;
	}else
	{
		cout << --n << endl;
	}
	//cout << n << endl;

	return 0;
}
