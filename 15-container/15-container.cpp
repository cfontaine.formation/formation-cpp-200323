
#include <iostream>
#include<vector>
#include <map>
#include <algorithm>

using namespace std;

void afficher(int val) {
    cout << val << endl;
}

bool impaire(int val) {
    return val % 2 != 0;
}

int main()
{
    vector<int> v;
    v.push_back(2);
    v.push_back(22);
    v.push_back(21);
    v.push_back(42);
    cout << v.size() << endl;
    cout << v[1] << endl;

    for (auto p : v) { // en C++11
        cout << p << endl;
    }

    for (auto it = v.begin(); it != v.end(); it++) {
        cout << *it << endl;
    }

    map<int, string> m;
    m[23] = "hello";
    m[2] = "world";
    cout << m[23] << endl;
    for (auto it = m.begin(); it != m.end(); it++) {
        cout << it->first <<" "<<it->second<< endl;
    }
    /*m.erase(2);
    m.clear();
    cout << m.empty() << endl;*/

    // algorithme
    vector<int>vi(10);
    fill(vi.begin() + 1, vi.end() - 1, 2);
    vi[0] = 3;
    vi[9] = 7;
    cout << "min=" << *(min_element(vi.begin(), vi.end())) << endl;
    for_each(vi.begin(),vi.end(),afficher);
    auto it2= find_if(vi.begin(), vi.end(), impaire);
    cout << *it2 << endl;
}
