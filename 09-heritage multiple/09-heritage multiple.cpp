// 09-heritage multiple.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
//

#include <iostream>
#include "Enfant.h"

int main()
{
	// Héritage multiple
	// 
	// Problème du diamant -> virtual
	//		Personne
	//    /        \
	// Pere			Mere
	//    \		   /
	//		Enfant
	Enfant  enfant("John");

	// Sans virtual au niveau de l'hétitage-> 2 objets Personne
	// avec virtual au niveau de l'hétitage-> 1 objet Personne Partagé par Pere et Mere
}

