#include <iostream>
#include <string>
#include "Pair.h"

using namespace std;

//template<class T> T somme(T a, T b)
template<typename T=int> T somme(T a, T b)
{
	return a + b;
}

template<typename T> void testStatic()
{
	static T stat;
	stat++;
	cout << stat << endl;
}

int main()
{
	// Fonction générique
	cout << somme<int>(1, 2) << endl;
	cout << somme(1L, 2L) << endl;
	cout << somme(1.3, 2.5) << endl;
	cout << somme(string("aze"), string("rty")) << endl;

	testStatic<int>();
	testStatic<int>();
	testStatic<int>();
	testStatic<double>();
	testStatic<double>();

	// Classe générique => un seul fichier .h
	Pair<int> p(23, 56);
	cout << p.minimum() << endl;
	Pair<string> ps("aze", "rty");
	cout << ps.minimum() << endl;
}
