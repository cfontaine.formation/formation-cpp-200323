#include <iostream>
//#include <climits>
using namespace std;

// En C++98
enum Direction { NORD = 90, SUD = 180, EST = 0, OUEST = 270 };

// problème typage des enum
enum Emballage { CARTON = 90, PLASTIQUE = 180, SANS = 0, PAPIER = 270 };

// En C++ 11 enum class
enum class Motorisation { ESSENCE = 95, DIESEL = 2, GPL = 10, ELECTRIQUE = 3 };

enum class DirectionO { NORD = 95, SUD = 2, EST = 10, OUEST = 3 };

int main()
{
	// Tableau (statique) à une dimension
	// Déclaration: type nom[taille]
	double t[5];

	// Initialisation du tableau
	for (int i = 0; i < 5; i++) {
		t[i] = 0.0;
	}

	// Accès à un élément du tableau
	t[0] = 3.4;
	cout << t[0] << endl;

	// int t2[4] = {}; // initialiser à la valeur 0 correspond à {0,0,0,0}
	// int t2[4] = { -4,7 }; // correspond à {-4,7,0,0}
	int t2[] = { -4,7,5 ,8 };// la taille du tableau correspond au nombre d'élément

	// Parcourir un tableau
	for (int i = 0; i < 4; i++) {
		cout << t2[i] << endl;
	}

	// Parcourir un tableau en C++11
	// Pas de modification des éléments du tableau
	for (auto v : t) { // uniquement en lecture
		cout << v << endl;
		v = 10.6;
	}

	// En utilisant une référence, on peut  modifier des éléments du tableau
	for (auto& v : t) {
		cout << v << endl;
		v = 10.6;
	}

	for (auto v : t) {
		cout << v << "\t";
	}
	cout << endl;

	// Calculer la taille d'un tableau (ne fonctionne pas avec les tableaux passés en paramètre de fonction
	cout << "Nombre d'element d'un tableau " << sizeof(t) / sizeof(t[0]) << endl;

	// la taille du tableau doit être une constante ou une litterale
	const int S = 4;
	int t3[S];
	//int t3[4];

	// Exercice : tableau
	// Trouver la valeur maximale et la moyenne d’un tableau de 5 entiers: -7, 4, 8, 0, -3
	int tab[] = { -7, 4, 8, 0, -3 };
	int max = tab[0]; // INT_MIN; //INT_MIN => correspond à la valeur minimale que peut prendre un entier se trouve dans climit
	int somme = 0.0;
	for (auto e : tab) {
		if (e > max) {
			max = e;
		}
		somme += e;
	}
	cout << "maximum=" << max << " moyenne=" << (static_cast<double>(somme) / 5) << endl;


	// Tableau à 2 dimensions
	// Déclaration
	// char t2d [3][2];

	// Déclaration est initialistion
	// char t2d[3][2] = {}; // initialisé à zéro
	// char t2d[3][2] = { {'a','z'},{'e','r'},{'r','t'} };
	char t2d[3][2] = { 'a','z','e','r','r','t' };

	// Accès à un élément
	t2d[0][1] = 'y';
	cout << t2d[0][0] << endl;

	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 2; j++) {
			cout << t2d[i][j] << '\t';
		}
		cout << endl;
	}

	int t2dInt[3][2] = { 2,5,3,7 - 4,6 };
	cout << t2dInt[1] << " " << &t2dInt[1][0] << endl;
	// Calculer le nombre  de colonnne  d'un tableau (ne fonctionne pas avec les tableaux passés en paramètre
	int nbColonne = sizeof(t2dInt[0]) / sizeof(t2dInt[0][0]); // 2
	// Calculer le nombre  de ligne  d'un tableau
	int nbLigne = sizeof(t2dInt) / sizeof(t2dInt[0]); //3
	// Calculer le nombre  d'élément  d'un tableau
	int nbElement = sizeof(t2dInt) / sizeof(t2dInt[0][0]); //6
	cout << "Nombre de colonne=" << nbColonne << " Nombre de ligne=" << nbLigne << " Nombre d'élément=" << nbElement << endl;


	// Pointeur
	// Déclaration d'un pointeur de double
	double* ptr;

	double val = 12.34;
	// &val => adresse de la variable val
	cout << val << "  adresse" << &val << endl;
	ptr = &val;
	cout << " pointeu=" << ptr << endl;

	// *ptr => Accès au contenu de la variable pointer par ptr
	cout << *ptr << endl; //12.34
	*ptr = 23.45;
	cout << *ptr << "  " << val << endl;

	// Un pointeur qui ne pointe sur rien
	double* ptr2 = 0; // 0 en C++ (en c NULL)
	double* ptr3 = nullptr; //en C++11

	// On peut affecter un pointeur avec un pointeur du même type
	ptr2 = ptr;		// ptr2 contient aussi l'adresse de val
	cout << ptr << "  " << ptr2 << " " << *ptr2 << endl;

	// On peut comparer 2 pointeurs du même type
	if (ptr == ptr2) {
		cout << "pointeurs égaux" << endl;
	}

	// ou comparer un pointeur à 0 ou nullptr
	if (!ptr3) {// ptr3 == 0 ou ptr3 == nullptr
		cout << "le pointeur est null" << endl;
	}

	// Pointeur constant
	// En préfixant un pointeur avec const => Le contenu de la variable pointée est constant
	const double* ptrCst = &val;
	cout << *ptrCst << endl; // 23.45
	//*ptrCst = 10.5;	// on ne peut plus modifier le contenu pointé par l'intermédiaire du pointeur
	ptrCst = nullptr;	// mais on peut modifier le pointeur

	// En postfixant un pointeur avec const => le pointeur est constant
	double* const ptrCstPost = &val;
	cout << *ptrCstPost << endl;
	*ptrCstPost = 45.6;			// On peut modofier le contenu pointé
	//ptrCstPost = nullptr;		// Le pointeur ptrCstPos est constant, on ne peut plus le modifier

	// En préfixant et en postfixant un pointeur avec const: le pointeur ptrCst2 est constant et
	// l'on ne peut plus modifier le contenu de la variable pointée par l'intermédiaire du pointeur
	const double* const ptrCst2 = &val;
	cout << *ptrCst2 << endl;
	//*ptrCst2 = 2.3;
	//ptrCst2 = nullptr;


	ptrCst = &val;
	// Opérateur cast -> supprimer le qualificatif const
	double* ptr4 = (double*)ptrCst; // en C
	*ptr4 = 9.6;
	cout << *ptr4 << endl;
	double* ptr5 = const_cast<double*>(ptrCst); // en C++, const_cast permet de supprimer le qualificatif const
	*ptr5 = 6.7;
	cout << *ptr5 << endl;

	// Exercice Pointeur
	// Créer
	// - 3 variables de type double: v1, v2, v3
	// - créer un pointeur de double : ptrChoix
	//	entrer une valeur entre 1 et 3 et  Afficher la valeur de la variable choisie par l'intermédiare du pointeur 

	double  v1 = 4, v2 = 4.5, v3 = 20;
	double* ptrChoix = nullptr;

	int choix;
	cout << "choix=";
	cin >> choix;
	switch (choix) {
	case 1:
		ptrChoix = &v1;
		break;
	case 2:
		ptrChoix = &v2;
		break;
	case 3:
		ptrChoix = &v3;
		break;
	}
	if (ptrChoix) {
		cout << *ptrChoix << endl;
	}

	// Tableau et pointeur
	int tp[] = { 65,-4,8,2 };
	cout << tp << endl;		// Le nom d'un tableau est un pointeur sur le premère élément
	cout << *tp << endl;	// On accède au premier élément du tableau

	// On accède au 3ème élément du tableau
	cout << tp + 2 << endl;
	cout << *(tp + 2) << " " << tp[2] << endl;

	int* ptrTp = tp;
	cout << *(ptrTp + 2) << " " << ptrTp[2] << endl;	// équivalent à t2[2]

	// Arithmétique des pointeurs
	cout << *ptrTp << endl; // 65
	ptrTp = ptrTp + 2;
	cout << *ptrTp << endl; // 8
	ptrTp++;
	cout << *ptrTp << endl; // 2
	cout << ptrTp - tp << endl; // 3

	// Pointeur void -> C
	ptrTp = tp;
	void* ptrVoid = ptrTp;
	// cout << *ptrVoid << endl; // *void -> ne permet pas le déférencement
	int* ptrTp3 = (int*)ptrVoid;
	cout << *ptrTp3 << endl;

	char* ptrTp4 = (char*)ptrVoid;
	cout << *ptrTp4 << endl;
	ptrTp4++;
	cout << *ptrTp4 << endl;

	// reinterpret_cast
	int rti = 0x61 | 0x64 << 8 | 0x55 << 16; // 00 55 64 61
	cout << hex << rti << endl;
	int* ptrRti = &rti;

	char* ptrChr = reinterpret_cast<char*>(ptrRti);
	cout << *ptrChr << " " << static_cast<int>(*ptrChr) << endl; // 61
	cout << *(ptrChr + 1) << " " << ptrChr[1] << " " << static_cast<int>(*(ptrChr + 1)) << endl; // 61
	cout << *(ptrChr + 2) << " " << ptrChr[2] << " " << static_cast<int>(*(ptrChr + 2)) << endl; // 61

	// Allocation dynamique de mémoire
	double* ptrDyn = new double;	// new => allocation de la mémoire
	*ptrDyn = 5.6;
	cout << dec << *ptrDyn << endl;
	cout << ptrDyn << endl;
	delete ptrDyn;					// delete => libération de la mémoire
	//cout << ptrDyn << endl;
	ptrDyn = nullptr;

	// Allocation dynamique avec initialisation C++98
	ptrDyn = new double(3.45);
	cout << *ptrDyn << endl;
	delete ptrDyn;

	// Allocation dynamique avec initialisation C++11
	ptrDyn = new double{ 6.7 };
	cout << *ptrDyn << endl;
	delete ptrDyn;

	// Pas de problème, si on utilise delete sur un pointeur null, il ne  se passe rien
	ptrDyn = nullptr;
	delete ptrDyn;

	// Allocation dynamique d'un tableau
	int size = 5;
	int* ptrTabDyn = new int[size];		// new [] => création d'un tableau dynamique
	*ptrTabDyn = 42;			// ptrTabDyn[0]=42;
	*(ptrTabDyn + 1) = 78;		// ptrTabDyn[1]=78;
	// On peut accèder à un tableau dynamique comme à un tableau statique
	ptrTabDyn[2] = 5;
	ptrTabDyn[3] = 34;
	ptrTabDyn[4] = 51;

	for (int i = 0; i < 5; i++) {
		cout << ptrTabDyn[i] << '\t' << endl;
	}
	delete[] ptrTabDyn;		// delete [] => libération d'un tableau dynamique
	ptrTabDyn = nullptr;

	// Exercice : Tableau dynamique
	// Modifier le programme Tableau pour faire la saisie :
	// - de la taille du tableau
	// - des éléments du tableau
	// Trouver la valeur maximale et la moyenne du tableau
	int sizeTab;
	cout << "Taille de tableau" << endl;
	cin >> sizeTab;
	if (sizeTab > 0) {
		int* tabDyn = new int[sizeTab];
		for (int i = 0; i < sizeTab; i++) {
			cout << "tab[" << i << "]=";
			cin >> tabDyn[i];
		}
		double maxVal = tabDyn[0];
		double sommeM = 0.0;
		for (int i = 0; i < sizeTab; i++)
		{
			if (tabDyn[i] > maxVal) {
				maxVal = tabDyn[i];
			}
			sommeM += tabDyn[i];
		}
		cout << "Moyenne=" << sommeM / sizeTab << " Maximum=" << maxVal << endl;
		delete[] tabDyn;
	}

	// Référence 
	// Une référence correspond à un autre nom que l'on donne à la variable
	// On doit aussi intialiser une référence avec une lvalue (= qui possède une adresse)
	int b = 123;
	int& ref = b;
	cout << b << "  " << ref << endl;
	ref = 4;
	cout << b << "  " << ref << endl;
	// double& ref2;		// Erreur pas d'initialisation
	// double& ref2 = 12.3;	// Erreur une référence ne pas être initialisé avec une littéral

	// Référence constante
	const int& refCst = b;
	cout << b << "  " << refCst << endl;
	b = 23;
	// refCst = 5;	// On ne peut plus utiliser une référence constante pour modifier la valeur

	// On peut initialiser une référence constante avec une valeur litérale
	const int& refCst2 = 56;
	cout << refCst2 << endl;

	// Enumération
	Direction dir = Direction::NORD; //NORD
	int vDir = dir;  // Convertion implicite enum -> int
	cout << dir << " " << vDir << endl;

	// Eviter la conversion int => enum
	//vDir = 120;
	//dir = (Direction)vDir;
	//cout << dir << endl;

	// problème avec les enums, il n'y pas de vérification de type
	dir = EST;
	Emballage emb = CARTON;
	if (dir == emb) {		// dir et emb sont considéré comme égal même si ceux sont des enumérations différente
		cout << "égal" << endl;
	}

	// enum class C++11
	Motorisation m = Motorisation::DIESEL;

	// avec enum class, il n'y a plus de convertion implicite enum -> int
	int vm = static_cast<int> (m);	// il faut utiliser un static_cast
	cout << vm << endl;

	DirectionO dirO = DirectionO::SUD;

	//if (m == dirO) {	// Erreur -> avec enum class il y a une vérification de type 
	//	cout << "égal" << endl;
	//}
}

